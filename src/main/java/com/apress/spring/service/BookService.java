package com.apress.spring.service;

import com.apress.spring.domain.Book;
import com.apress.spring.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by justice on 10/31/16.
 */
@Service
public class BookService {
    @Autowired
    BookRepository repo;

    public void save(Book book){
        this.repo.save(book);
    }
}
