package com.apress.spring.service;

import com.apress.spring.domain.Book;
import com.apress.spring.domain.Journal;
import com.apress.spring.domain.Product;
import com.apress.spring.repository.BookRepository;
import com.apress.spring.repository.JournalRepository;
import com.apress.spring.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by justice on 11/8/16.
 */
@Service
public class ProductService {
    @Autowired
    ProductRepository repo;

    public Product save(Product product){
        return this.repo.save(product);
    }

    public List<Product> findAll(){
        return this.repo.findAll();
    }

    public Product findByProductId(Integer Id){
        return this.repo.findById(Id);
    }
}
