package com.apress.spring.domain;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by justice on 10/30/16.
 */
@Entity
@Table(name = "BOOK")
@NamedQueries({
        @NamedQuery(name = "Book.namedQueryAllBooks", query="select b from Book b"),
        @NamedQuery(name = "Book.namedQueryGreaterThanPages", query="select b from Book b where b.pageCount > ?1"),
        @NamedQuery(name = "Book.namedQueryWithTitleEquals", query="select b from Book b where b.title = :title"),
})
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;
    private String title;
    private Date publishDate;
    private int pageCount;
    private BigDecimal price;

    public Book() {
    }

    public Book(String title, Date publishDate, int pageCount, BigDecimal price) {
        this.title = title;
        this.publishDate = publishDate;
        this.pageCount = pageCount;
        this.price = price;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String toString(){
        return getTitle() + " with published date " + getPublishDate();
    }

}
