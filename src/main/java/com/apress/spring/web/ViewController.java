package com.apress.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jchaudhary on 10/18/16.
 */
@Controller
public class ViewController {
    @RequestMapping("/home")
    public String viewHome(Model model){
        return "home";
    }

    @RequestMapping("/test")
    public String viewTest(Model model){
        return "test";
    }
}
