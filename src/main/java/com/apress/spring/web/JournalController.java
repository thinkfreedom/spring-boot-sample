package com.apress.spring.web;

/**
 * Created by jchaudhary on 8/31/16.
 */
import com.apress.spring.repository.JournalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
//import com.apress.spring.repository.JournalRepository;
@Controller
public class JournalController {
    @Autowired
    JournalRepository journalRepository;

    @RequestMapping("/journal")
    public String index(Model model) {
        model.addAttribute("journal", journalRepository.findAll());
        return "index";
    }
}