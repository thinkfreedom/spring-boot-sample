package com.apress.spring.web;

import com.apress.spring.domain.Product;
import com.apress.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by justice on 10/16/16.
 */
@Controller
public class ProductController {
    @Autowired
    private ProductService productService;

//    @Autowired
//    public void setProductService(ProductService productService) {
//        this.productService = productService;
//    }

    @RequestMapping("/products")
    public String listProducts(Model model){
        model.addAttribute("products", productService.findAll());
        return "products";
    }

    @RequestMapping("/product/{id}")
    public String getProductById(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.findByProductId(id));
        return "product";
    }

    @RequestMapping("/product/new")
    public String newProduct( Model model){
        model.addAttribute("product", new Product());
        return "productform";
    }

    @RequestMapping("/product/edit/{id}")
    public String editProduct(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.findByProductId(id));
        return "productform";
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public String saveOrUpdateProduct(Product product){
        Product savedProduct = productService.save(product);
        return "redirect:/product/" + savedProduct.getId();
    }
}
