package com.apress.spring;

import com.apress.spring.domain.Book;
import com.apress.spring.domain.Customer;
import com.apress.spring.domain.Journal;
//import com.apress.spring.repository.JournalRepository;
import com.apress.spring.domain.Product;
import com.apress.spring.repository.BookRepository;
import com.apress.spring.repository.CustomerRepository;
import com.apress.spring.repository.JournalRepository;
import com.apress.spring.repository.ProductRepository;
import com.apress.spring.service.BookService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class SpringTestApplication {
    private static final Logger log = LoggerFactory.getLogger(SpringTestApplication.class);

    @Bean
    InitializingBean saveData(JournalRepository repo){
        return () -> {
            repo.save(new Journal("Get to know Spring Boot","Today I will learn Spring Boot","01/01/2016"));
            repo.save(new Journal("Simple Spring Boot Project","I will do my first Spring Boot Project","01/02/2016"));
            repo.save(new Journal("Spring Boot Reading","Read more about Spring Boot","02/01/2016"));
            repo.save(new Journal("Spring Boot in the Cloud","Spring Boot using Cloud Foundry","03/01/2016"));
        };
    }

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringTestApplication.class, args);
        System.out.println("*************         Beans          *************");
        System.out.println(context.getBeanDefinitionCount());
//        BookService bookService = context.getBean(BookService.class);
//        bookService.save(new Book("Apple", new Date(), 9, new BigDecimal(55)));
        System.out.println("*************Application fully loaded*************");
    }

    @Bean
    public CommandLineRunner testProductRepository(ProductRepository repository) {
        return (args) -> {
            // save a couple of products
            repository.save(new Product("This is first product", new BigDecimal(10), "http://www.products.com/product/1"));
            repository.save(new Product("This is second product", new BigDecimal(20), "http://www.products.com/product/2"));
            repository.save(new Product("This is third product", new BigDecimal(30), "http://www.products.com/product/3"));
            repository.save(new Product("This is fourth product", new BigDecimal(40), "http://www.products.com/product/4"));
            repository.save(new Product("This is fifth product", new BigDecimal(50), "http://www.products.com/product/5"));
        };
    }

    @Bean
    public CommandLineRunner testBookRepository(BookRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Book("Of Mice and Men", new Date(), 54, new BigDecimal(34.00)));
            repository.save(new Book("One Flew Over the Cuckoos Nest", new Date(), 54, new BigDecimal(77.00)));
            repository.save(new Book("For Whom the Bell Tolls", new Date(), 54, new BigDecimal(72.00)));
            repository.save(new Book("War and Peace", new Date(), 54, new BigDecimal(77.00)));
            repository.save(new Book("The Grapes of Wrath", new Date(), 54, new BigDecimal(78.00)));
            repository.save(new Book("A Tale of Two Cities", new Date(), 54, new BigDecimal(79.00)));

            /*
            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Book book : repository.findAll()) {
                log.info(book.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            Book book = repository.findOne(1L);
            log.info("Book found with findOne(1L):");
            log.info("--------------------------------");
            log.info(book.toString());
            log.info("");

            // fetch customers by last name
            log.info("Book found with findByTitle('A Tale of Two Cities'):");
            log.info("--------------------------------------------");
            for (Book fbook : repository.findByTitle("A Tale of Two Cities")) {
                log.info(fbook.toString());
            }
            log.info("");

            // fetch books  by query
            log.info("Book found with findByTitle('A Tale of Two Cities'):");
            log.info("--------------------------------------------");
            for (Book fbook : repository.queryWithTitleEquals("A Tale of Two Cities")) {
                log.info(fbook.toString());
            }
            log.info("");
            // fetch books  by query
            log.info("Book found with queryAllBooks():");
            log.info("--------------------------------------------");
            for (Book fbook : repository.queryAllBooks()) {
                log.info(fbook.toString());
            }
            log.info("");
            // fetch books  by query
            log.info("Book found with queryGreaterThanPages(20)");
            log.info("--------------------------------------------");
            for (Book qbook : repository.queryGreaterThanPages(20)) {
                log.info(qbook.toString());
            }
            log.info("");

            // fetch books  by named query
            log.info("\n\n\n=======Named Query=======");
            log.info("Book found with namedQueryWithTitleEquals('A Tale of Two Cities'):");
            log.info("--------------------------------------------");
            for (Book fbook : repository.namedQueryWithTitleEquals("A Tale of Two Cities")) {
                log.info(fbook.toString());
            }
            log.info("");
            // fetch books  by query
            log.info("Book found with namedQueryAllBooks():");
            log.info("--------------------------------------------");
            for (Book fbook : repository.namedQueryAllBooks()) {
                log.info(fbook.toString());
            }
            log.info("");
            // fetch books  by query
            log.info("Book found with namedQueryGreaterThanPages(20)");
            log.info("--------------------------------------------");
            for (Book qbook : repository.namedQueryGreaterThanPages(20)) {
                log.info(qbook.toString());
            }
            log.info("");


            log.info("Paging Request Test");
            log.info("--------------------------------------------");
            for (Book book : repository.pageCountGreaterThan(10,new PageRequest(0,2))) {
                log.info(book.toString());
            }
            log.info("");
            */
        };
    }

    @Bean
    public CommandLineRunner demo(CustomerRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Customer("Jack", "Bauer"));
            repository.save(new Customer("Chloe", "O'Brian"));
            repository.save(new Customer("Kim", "Bauer"));
            repository.save(new Customer("David", "Palmer"));
            repository.save(new Customer("Michelle", "Dessler"));

            /*
            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Customer customer : repository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            Customer customer = repository.findOne(1L);
            log.info("Customer found with findOne(1L):");
            log.info("--------------------------------");
            log.info(customer.toString());
            log.info("");

            // fetch customers by last name
            log.info("Customer found with findByLastName('Bauer'):");
            log.info("--------------------------------------------");
            for (Customer bauer : repository.findByLastName("Bauer")) {
                log.info(bauer.toString());
            }
            log.info("");
            */
        };
    }
}
