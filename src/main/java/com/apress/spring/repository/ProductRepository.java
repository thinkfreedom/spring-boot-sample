package com.apress.spring.repository;

import com.apress.spring.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by justice on 11/8/16.
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAll();
    Page<Product> findAll(Pageable pageable);
    Product findById(Integer Id);
}
