package com.apress.spring.repository;

import com.apress.spring.domain.Book;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.yaml.snakeyaml.events.Event;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.List;

/**
 * Created by justice on 10/30/16.
 */


public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findByTitle(String title);

    @Query("select b from Book b")
    public List<Book> queryAllBooks();

    @Query("select b from Book b where b.pageCount > ?1")
    public List<Book> queryGreaterThanPages(int pageCount);

    @Query("select b from Book b where b.title = :title")
    public List<Book> queryWithTitleEquals(@Param("title") String title);

    public List<Book> namedQueryAllBooks();

    public List<Book> namedQueryGreaterThanPages(int pageCount);
    public List<Book> pageCountGreaterThan(int pageCount, Pageable pageable);

    public List<Book> namedQueryWithTitleEquals(@Param("title") String title);

}
