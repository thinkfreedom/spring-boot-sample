package com.apress.spring.repository;

import com.apress.spring.domain.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jchaudhary on 10/25/16.
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByFirstName(String firstName);
    List<Customer> findByLastName(String lastName);
}
