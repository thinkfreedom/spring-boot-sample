package com.apress.spring.repository;

/**
 * Created by jchaudhary on 8/31/16.
 */
import org.springframework.data.jpa.repository.JpaRepository;
import com.apress.spring.domain.Journal;
import org.springframework.stereotype.Repository;

@Repository
public interface JournalRepository extends JpaRepository<Journal, Long> {}